const { override, addWebpackAlias } = require('customize-cra');
const path = require('path');

module.exports = override(
  addWebpackAlias({
    'assets': path.resolve(__dirname, './src/assets'),
    'components': path.resolve(__dirname, './src/components'),
    'constants': path.resolve(__dirname, './src/constants'),
    'hooks': path.resolve(__dirname, './src/hooks'),
    'modules': path.resolve(__dirname, './src/modules'),
    'scss': path.resolve(__dirname, './src/scss'),
    'utils': path.resolve(__dirname, './src/utils'),
    'views': path.resolve(__dirname, './src/views'),
  })
);
