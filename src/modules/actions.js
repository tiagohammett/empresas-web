// types of Actions
export const actionTypes = {
  SET_FETCHING: 'SET_FETCHING',
  SET_ENTERPRISES: 'SET_ENTERPRISES',
  SET_SEARCH: 'SET_SEARCH',
  SET_AUTH: 'SET_AUTH',
};

// Actions
export const setIsFetching = isFetching => ({
  type: actionTypes.SET_FETCHING,
  isFetching
});

export const setEnterprises = enteprisesArray => ({
  type: actionTypes.SET_ENTERPRISES,
  enterprises: enteprisesArray
});

export const setSearch = searchTerm => ({
  type: actionTypes.SET_SEARCH,
  searchTerm
});

export const setAuth = authObj => ({
  type: actionTypes.SET_AUTH,
  authObj
});
