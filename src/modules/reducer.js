import { actionTypes } from 'modules/actions';
import { cloneDeep } from 'lodash';

const { SET_FETCHING, SET_ENTERPRISES, SET_SEARCH, SET_AUTH } = actionTypes;

const defaultEmptyAuth = {
  accessToken: '',
  client: '',
  uid: '',
};

const defaultState = {
  isFetching: false,
  enterprises: [],
  searchTerm: '',
  auth: defaultEmptyAuth
};

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_FETCHING: {
      let newState = cloneDeep(state);
      newState.isFetching = !!action.isFetching;
      return newState;
    }

    case SET_ENTERPRISES: {
      let newState = cloneDeep(state);
      newState.enterprises = action.enterprises;
      return newState;
    }

    case SET_SEARCH: {
      let newState = cloneDeep(state);
      newState.searchTerm = action.searchTerm;
      return newState;
    }

    case SET_AUTH: {
      let newState = cloneDeep(state);
      const { authObj } = action;
      newState.auth = typeof authObj === 'object' ? authObj : defaultEmptyAuth;
      return newState;
    }

    default:
      return state;
  }
};

export default reducer;
