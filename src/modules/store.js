import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from 'modules/reducer';

const composedEnhancers = composeWithDevTools(applyMiddleware(thunk));

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['auth'] // persist user's info tokens on refresh
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = createStore(persistedReducer, composedEnhancers);
export const persistor = persistStore(store);
