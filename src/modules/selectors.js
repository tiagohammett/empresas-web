export const isLoadingSelector = state => state.isFetching;

export const searchTermSelector = state => state.searchTerm;

export const authObjSelector = state => state.auth;

export const isLoggedInSelector = state => {
  const { accessToken, uid } = state.auth;
  return !!accessToken && !!uid;
};

export const enterprisesSelector = state => state.enterprises;

export const entepriseDetailsSelector = state => id => state.enterprises.find(item => item.id === parseInt(id));
