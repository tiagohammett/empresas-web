import axios from 'axios';
import SERVICE from 'constants/service';
import { setIsFetching, setEnterprises, setAuth } from 'modules/actions';
import { searchTermSelector } from 'modules/selectors';
import { setAuthHeader } from 'utils/services';

const { API_BASE_URL, PATHS } = SERVICE;
export const ServerAPI = axios.create({ baseURL: API_BASE_URL });

export const login = (payload, handleError) =>
  dispatch => {
    dispatch(setIsFetching(true));

    ServerAPI
      .post(PATHS.LOGIN, payload)
      .then(response => {
        const { headers } = response;
        const { client, uid } = headers || {};
        const authObj = {
          accessToken: headers['access-token'],
          client,
          uid
        };

        setAuthHeader(authObj);
        dispatch(setAuth(authObj));
        dispatch(setIsFetching(false));
      })
      .catch(error => {
        const { errors } = error?.response?.data || {};
        !!handleError ? handleError() : alert(errors.join('\n'));
        dispatch(setIsFetching(false));
      });
  };

export const searchEnterprises = () =>
  (dispatch, getState) => {
    const state = getState();
    const searchTerm = searchTermSelector(state);
    const params = {}
    if (searchTerm) params.name = searchTerm;

    dispatch(setIsFetching(true));
    ServerAPI
      .get(PATHS.ENTERPRISES, { params })
      .then(response => {
        const { data } = response;
        dispatch(setEnterprises(data?.enterprises));
        dispatch(setIsFetching(false));
      })
      .catch(error => {
        const { errors } = error?.response?.data || {};
        alert(errors?.join('\n'));
        dispatch(setIsFetching(false));
      });
  };
