import { ServerAPI } from 'modules/services';
import SERVICES from 'constants/service';

export const setAuthHeader = authObj => {
  const { accessToken, client, uid } = authObj;
  ServerAPI.defaults.headers.common = {
    ...ServerAPI.defaults.headers.common,
    'access-token': accessToken,
    client,
    uid,
  };
}

export const getImageSrc = imageUrl => imageUrl ? `${SERVICES.BASE_URL}${imageUrl}` : 'https://source.unsplash.com/600x300/?enterprises';
