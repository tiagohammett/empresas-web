const BASE_URL = 'https://empresas.ioasys.com.br';

const SERVICE = {
  BASE_URL,
  API_BASE_URL: `${BASE_URL}/api/v1/`,
  PATHS: {
    LOGIN: 'users/auth/sign_in',
    ENTERPRISES: 'enterprises',
  }
}

export default SERVICE;
