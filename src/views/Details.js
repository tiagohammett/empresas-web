import React, { useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { isEmpty } from 'lodash';
import { entepriseDetailsSelector } from 'modules/selectors';
import HeaderBar from 'components/HeaderBar';
import Container from 'components/Container';
import EnterpriseDetailsCard from 'components/EnterpriseDetailsCard';
import BackArrow from 'assets/left-arrow.svg';
import 'scss/views/Details.scss';

const Details = () => {
  const { id } = useParams();
  const details = useSelector(entepriseDetailsSelector)(id);

  useEffect(() => {
    if (isEmpty(details)) {
      window.location.assign('/empresas');
    }
  }, [details]);

  return (
    <>
      <HeaderBar leftAligned>
        <Link to='/empresas' className='enterprise-details__header-link'>
          <img
            className='enterprise-details__header-link-icon'
            src={BackArrow}
            role='presentation'
            aria-hidden='true'
          />
        </Link>
        <h1 className='enterprise-details__header-title'>{details?.enterprise_name}</h1>
      </HeaderBar>
      <Container>
        <EnterpriseDetailsCard enterprise={details} />
      </Container>
    </>
  )
};

export default Details;
