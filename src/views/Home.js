import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { isLoggedInSelector } from 'modules/selectors';
import ResponsiveImage from 'components/ResponsiveImage';
import LoginForm from 'components/LoginForm';
import Logo from 'assets/logo-home.png';
import Logo2x from 'assets/logo-home@2x.png';
import Logo3x from 'assets/logo-home@3x.png';
import 'scss/views/Home.scss';


const Home = () => {
  const isLoggedIn = useSelector(isLoggedInSelector);

  useEffect(
    () => {
      if (isLoggedIn) window.location.assign('/empresas');
    },
    [isLoggedIn]
  );

  return (
    <div className='home'>
      <div className='home__container'>
        <ResponsiveImage
          src={Logo}
          src2x={Logo2x}
          src3x={Logo3x}
          className='home__logo'
        />
        <h1 className='home__title'>Bem vindo ao empresas</h1>
        <p className='home__subtitle'>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
        <LoginForm />
      </div>
    </div>
  )
};

export default Home;
