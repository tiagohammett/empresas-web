import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { searchTermSelector } from 'modules/selectors';
import ListingHeader from 'components/ListingHeader';
import EnterprisesList from 'components/EnterprisesList';
import 'scss/views/Listing.scss';

const Listing = () => {
  const searchTerm = useSelector(searchTermSelector);
  const [showList, setShowList] = useState(!!searchTerm);

  return (
    <>
      <ListingHeader onExpand={() => setShowList(true)} />
      {showList ? <EnterprisesList /> : <p className='listing__disclaimer'>Clique na busca para iniciar.</p>}
    </>
  )
};

export default Listing;
