import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import useAuthRehydrate from 'hooks/useAuthRehydrate';

import Details from 'views/Details';
import Listing from 'views/Listing';
import Home from 'views/Home';
import LoadingWidget from 'components/LoadingWidget';
import 'scss/global.scss';

function App() {
  useAuthRehydrate();

  return (
    <Router>
      <LoadingWidget />
      <Switch>
        <Route path='/empresas/:id' component={Details} />
        <Route path='/empresas' component={Listing} />
        <Route path='/' component={Home} />
      </Switch>
    </Router>
  );
}

export default App;
