import React, { useState } from 'react';
import cn from 'classnames';
import useActions from 'hooks/useActions';
import { login } from 'modules/services';
import EyeIcon from 'assets/eye-solid.svg';
import EyeSlashIcon from 'assets/eye-slash-solid.svg';
import 'scss/components/LoginForm.scss';

const LoginForm = () => {
  const handleLogin = useActions(login);
  const [formData, updateFormData] = useState({});
  const [hiddenPassword, setHiddenPassword] = useState(true);
  const [haveErrors, setHaveErrors] = useState(false);

  const onSubmit = e => {
    e.preventDefault();
    setHaveErrors(false);
    handleLogin(formData, () => setHaveErrors(true));
  }

  const handleInputChange = e => {
    const { target } = e;
    updateFormData({
      ...formData,
      [target.name]: target.value
    });
  }

  return (
    <form className={cn('login-form', { 'login-form--with-errors': haveErrors })} onSubmit={onSubmit}>
      <div className='login-form__input-set login-form__input-set--email'>
        <input
          className='login-form__input'
          name='email'
          type='email'
          placeholder='E-mail'
          onChange={handleInputChange}
          required
        />
      </div>

      <div className='login-form__input-set login-form__input-set--password'>
        <input
          className='login-form__input'
          name='password'
          type={hiddenPassword ? 'password' : 'text'}
          placeholder='Senha'
          onChange={handleInputChange}
          required
        />
        <button
          className='login-form__input-button'
          type='button'
          onClick={() => setHiddenPassword(!hiddenPassword)}
          aria-label={hiddenPassword ? 'Mostrar Senha' : 'Esconder Senha'}
        >
          <img
            className='login-form__input-button-icon'
            src={hiddenPassword ? EyeIcon : EyeSlashIcon}
            role='presentation'
            aria-hidden='true'
          />
        </button>
      </div>

      {haveErrors && <p className='login-form__error-copy'>Credenciais informadas são inválidas, tente novamente.</p>}

      <button className='login-form__submit-button' type='submit'>Entrar</button>
    </form>
  )
};

export default LoginForm;
