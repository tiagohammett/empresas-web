import React from 'react';
import { useSelector } from 'react-redux';
import { isLoadingSelector } from 'modules/selectors';
import LoadingSvg from 'assets/loading.svg';
import 'scss/components/LoadingWidget.scss';

const LoadingWidget = () => {
  const isLoading = useSelector(isLoadingSelector);

  return isLoading &&
    <div className='loading-widget'><img src={LoadingSvg} alt='Carregando' /></div>;
}

export default LoadingWidget;
