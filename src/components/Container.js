import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import 'scss/components/Container.scss';

const Container = ({ children, className }) => <div className={cn('container', className)}>{children}</div>;

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  className: PropTypes.string,
}

export default Container;
