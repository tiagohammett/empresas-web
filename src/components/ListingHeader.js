import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { debounce } from 'lodash';
import { searchTermSelector } from 'modules/selectors';
import useActions from 'hooks/useActions';
import { setSearch } from 'modules/actions';
import { searchEnterprises } from 'modules/services';
import HeaderBar from 'components/HeaderBar';
import ResponsiveImage from 'components/ResponsiveImage';
import SearchIcon from 'assets/ic-search.svg';
import Logo from 'assets/logo-nav.png';
import Logo2x from 'assets/logo-nav@2x.png';
import Logo3x from 'assets/logo-nav@3x.png';
import 'scss/components/ListingHeader.scss';

const ListingHeader = ({ onExpand }) => {
  const setSearchTerm = useActions(setSearch);
  const searchEnterprisesHandler = useActions(searchEnterprises);
  const searchTerm = useSelector(searchTermSelector);
  const [isExpanded, setExpanded] = useState(!!searchTerm);
  const searchInputRef = useRef();

  useEffect(() => {
    if (searchInputRef?.current) searchInputRef.current.focus();
  }, [searchInputRef.current, isExpanded]);

  const expandSearch = () => {
    setExpanded(true);
    onExpand?.();
  }

  const handleSearchChange = debounce(e => {
    const { value } = e?.target;
    setSearchTerm(value);
    searchEnterprisesHandler();
  }, 1000);

  const headerDefaultState =
    <>
      <ResponsiveImage
        src={Logo}
        src2x={Logo2x}
        src3x={Logo3x}
        className='listing-header__logo'
      />
      <button
        className='listing-header__toggle'
        type='button'
        onClick={expandSearch}
        aria-label={'Mostrar Pesquisa'}
      >
        <img
          className='listing-header__button-icon'
          src={SearchIcon}
          role='presentation'
          aria-hidden='true'
        />
      </button>
    </>;

  return (
    <HeaderBar>
      {
        !isExpanded ? headerDefaultState : (
          <form className='listing-header__form'>
            <img
              className='listing-header__button-icon'
              src={SearchIcon}
              role='presentation'
              aria-hidden='true'
            />
            <input
              name='search'
              type='search'
              ref={searchInputRef}
              className='listing-header__form-input'
              onChange={handleSearchChange}
              defaultValue={searchTerm}
              placeholder='Pesquisar'
            />
          </form>
        )
      }
    </HeaderBar>
  )
};

ListingHeader.propTypes = {
  onExpand: PropTypes.func
}

export default ListingHeader;
