import React from 'react';
import PropTypes from 'prop-types';
import { getImageSrc } from 'utils/services';
import 'scss/components/EnterpriseDetailsCard.scss';

const EnterpriseDetailsCard = ({ enterprise }) => {
  const {
    photo,
    enterprise_name,
    description
  } = enterprise || {};

  return enterprise ? (
    <div className='enterprise-details'>
      <img
        src={getImageSrc(photo)}
        alt={enterprise_name}
        className='enterprise-details__image'
      />
      <p className='enterprise-details__text'>{description}</p>
    </div>
  ) : null;
};

EnterpriseDetailsCard.propTypes = {
  enterprise: PropTypes.shape({
    photo: PropTypes.string,
    enterprise_name: PropTypes.string,
    description: PropTypes.string,
  })
}

export default EnterpriseDetailsCard;
