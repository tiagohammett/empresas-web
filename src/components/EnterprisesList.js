import React from 'react';
import { isEmpty } from 'lodash';
import { useSelector } from 'react-redux';
import { isLoadingSelector, enterprisesSelector } from 'modules/selectors';
import Container from 'components/Container';
import EnterpriseCard from 'components/EnterpriseCard';
import 'scss/components/EnterprisesList.scss';

const EnterprisesList = () => {
  const isLoading = useSelector(isLoadingSelector);
  const enterprises = useSelector(enterprisesSelector);
  const isEmptyList = isEmpty(enterprises);

  return !isLoading && (
    <Container>
      {isEmptyList ?
        <p className='enterprises-list__empty'>Nenhuma empresa foi encontrada para a busca realizada.</p> :
        <div className='enterprises-list__wrapper'>
          {enterprises.map(enterprise =>
            <div className='enterprises-list__item' key={enterprise.id}>
              <EnterpriseCard enterprise={enterprise} />
            </div>
          )}
        </div>
      }
    </Container>
  )
}

export default EnterprisesList;
