import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getImageSrc } from 'utils/services';
import 'scss/components/EnterpriseCard.scss';

const EnterpriseCard = ({ enterprise }) => {
  const {
    id,
    photo,
    enterprise_name,
    enterprise_type,
    city,
    country
  } = enterprise;

  return (
    <Link
      className='enterprise-card'
      to={`/empresas/${id}`}
    >
      <img
        src={getImageSrc(photo)}
        alt={enterprise_name}
        className='enterprise-card__image'
      />
      <div className='enterprise-card__details'>
        <h3 className='enterprise-card__title'>{enterprise_name}</h3>
        <p className='enterprise-card__type'>{enterprise_type.enterprise_type_name}</p>
        <p className='enterprise-card__location'>{city} {country}</p>
      </div>
    </Link>
  )
};

EnterpriseCard.propTypes = {
  enterprise: PropTypes.shape({
    id: PropTypes.number,
    enterprise_name: PropTypes.string,
    city: PropTypes.string,
    country: PropTypes.string,
    photo: PropTypes.string,
    enterprise_type: PropTypes.shape({
      enterprise_type_name: PropTypes.string,
    }),
  })
}

export default EnterpriseCard;
