import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Container from 'components/Container';
import 'scss/components/HeaderBar.scss';

const HeaderBar = ({ children, leftAligned }) => (
  <header className='header-bar'>
    <Container className={cn('header-bar__container', { 'header-bar__container--left-align': leftAligned })}>
      {children}
    </Container>
  </header>
);

HeaderBar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  leftAligned: PropTypes.bool
}

export default HeaderBar;
