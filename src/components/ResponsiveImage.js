import React from 'react';
import PropTypes from 'prop-types';

const ResponsiveImage = ({ src, src2x, src3x, alt, className }) =>
  <img
    alt={alt}
    className={className}
    src={src}
    srcSet={`${src}, ${src2x} 2x, ${src3x} 3x`}
  />;

ResponsiveImage.propTypes = {
  src: PropTypes.string,
  src2x: PropTypes.string,
  src3x: PropTypes.string,
  alt: PropTypes.string,
  className: PropTypes.string
}

ResponsiveImage.defaultProps = {
  alt: 'ioasys'
}

export default ResponsiveImage;
