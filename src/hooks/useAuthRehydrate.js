import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { setAuthHeader } from 'utils/services';
import { authObjSelector, isLoggedInSelector } from 'modules/selectors';

const useAuthRehydrate = () => {
  const authObj = useSelector(authObjSelector);
  const isLoggedIn = useSelector(isLoggedInSelector);

  return useEffect(
    () => {
      if (isLoggedIn) setAuthHeader(authObj);
    },
    []
  );
};

export default useAuthRehydrate;
